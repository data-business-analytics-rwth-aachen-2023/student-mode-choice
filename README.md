# Data Sets

A description of the corresponsing data set can be found in the papers mentioned below

## Student Mode Choice
Tscharaktschiew, S., Müller, S. (2021): Ride to the hills, ride to your school: Physical effort and mode choice. Transportation Research D 98, 102983. 

Müller, S., Mejía Dorantes, L., Kersten, E. (2020): Analysis of active school transportation in hilly urban environments: a case study of Dresden. Journal of Transport Geography (88), 102872.

Müller, S., Tscharaktschiew, S., Haase, K. (2008): Travel-to-school mode choice modelling and patterns of school choice in urban areas. Journal of Transport Geography 16(5), 342-357.






